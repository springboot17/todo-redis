package com.apress.todoredis.model;

public class ToDoBuilder {
    private String name;
    private String description;

    public ToDoBuilder setName(String name) {
        this.name = name;
        return this;
    }
    public ToDoBuilder setDesc(String desc) {
        this.description = desc;
        return this;
    }
    public ToDo build() {
        ToDo toDo = new ToDo();
        toDo.setName(this.name);
        toDo.setDescription(this.description);
        return toDo;
    }

}
