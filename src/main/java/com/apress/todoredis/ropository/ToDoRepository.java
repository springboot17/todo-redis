package com.apress.todoredis.ropository;


import org.springframework.data.repository.CrudRepository;

import com.apress.todoredis.model.ToDo;

public interface ToDoRepository extends CrudRepository<ToDo,String> {}