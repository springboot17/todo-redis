package com.apress.todoredis.config;

import com.apress.todoredis.model.ToDoBuilder;
import com.apress.todoredis.services.ToDoProducer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ToDoSender {

    @Bean
    CommandLineRunner sendMessage(
            ToDoProducer producer, @Value("${todo.redis.topic}")
            String topic) {
        return args -> {
            producer.send(topic, new ToDoBuilder().setName("sadas").setDesc("asdasd").build());
        };
    }
}
