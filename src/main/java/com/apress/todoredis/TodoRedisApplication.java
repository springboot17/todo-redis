package com.apress.todoredis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@SpringBootApplication
public class TodoRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(TodoRedisApplication.class, args);
    }

}
