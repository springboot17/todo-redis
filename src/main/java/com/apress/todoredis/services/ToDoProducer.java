package com.apress.todoredis.services;

import com.apress.todoredis.model.ToDo;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
@Log4j2
@AllArgsConstructor
public class ToDoProducer {
    private final RedisTemplate redisTemplate;
    public void send(String topic, ToDo toDo) {
        log.info("Producer > Todo send");
        this.redisTemplate.convertAndSend(topic, toDo);
    }
}
