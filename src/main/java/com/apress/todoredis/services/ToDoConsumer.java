package com.apress.todoredis.services;

import com.apress.todoredis.model.ToDo;
import com.apress.todoredis.ropository.ToDoRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Log4j2
public class ToDoConsumer {
    private final ToDoRepository toDoRepository;

    /**
     * обязательный метод для листенера
     * @param toDo {@link ToDo}
     */
    public void handleMessage(ToDo toDo) {
        log.info("todo save == > {}", toDo);
        log.info("Todo create == > {}", this.toDoRepository.save(toDo));
    }
}
